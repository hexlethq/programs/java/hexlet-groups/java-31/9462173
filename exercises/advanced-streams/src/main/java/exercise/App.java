package exercise;

import java.util.List;
import java.util.stream.Collectors;
import java.util.Arrays;

// BEGIN
class App {
    public static String getForwardedVariables(String str) {
        List<String> list = List.of(prepare(str).split(","));
        String result = list.stream()
                .filter(x -> x.startsWith("X_FORWARDED_"))
                .map(x -> x.replaceAll("X_FORWARDED_", ""))
                .collect(Collectors.joining(","));
        return result.replaceAll("\"", "");
    }

    public static String prepare(String str) {
        List<String> list = List.of(str.split("\n"));
        return list.stream()
                .filter(x -> x.contains("X_FORWARDED_"))
                .map(x -> x.replaceAll("environment=\"", ""))
                .collect(Collectors.joining(","));
    }
}
//END
