package exercise.servlet;


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;
import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {
        // BEGIN
        String companiesResponse = "Companies not found";
        String param = request.getParameter("search");
        String query = request.getQueryString();
        List<String> companies = getCompanies();
        //В случае отсутствия в строке запроса параметра `search` или если значением является пустая строка — должны выводиться все компании.
        if (query == null || param  == "") {
            companiesResponse = String.join("\n", companies);
        }

        List<String> temp = new ArrayList<>();
        for (String company : companies) {
            //Если строка запроса содержит параметр `search`, то должен выводиться список только тех компаний, которые содержат в имени переданное значение.
            if (query != null && company.contains(param)) {
                temp.add(company);
            }
        }
        if (!temp.isEmpty()) {
            companiesResponse = String.join("\n", temp);
        }
        PrintWriter printWriter = response.getWriter();
        printWriter.write(companiesResponse);
        printWriter.close();
        // END
    }
}
