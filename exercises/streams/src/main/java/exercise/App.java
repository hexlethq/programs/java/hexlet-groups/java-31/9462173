package exercise;

import java.util.List;
import java.util.Arrays;

// BEGIN
class App {
    public static long getCountOfFreeEmails(List<String> emails) {
        long count = emails.stream()
                .filter(x -> x.endsWith("gmail.com") || x.endsWith("yandex.ru") || x.endsWith("hotmail.com"))
                .count();
        return count;
    }
}
// END
