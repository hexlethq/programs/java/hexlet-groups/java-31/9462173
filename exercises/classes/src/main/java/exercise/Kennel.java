package exercise;

import java.util.Arrays;


// BEGIN
class Kennel {
    private static String[][] kennelPuppies = new String[][]{};
    private static int countPuppies = 0;

    public static void addPuppy(String[] newPuppie) {
        kennelPuppies = Arrays.copyOf(kennelPuppies, kennelPuppies.length + 1);
        kennelPuppies[kennelPuppies.length - 1] = newPuppie;
        countPuppies++;
    }

    public static void addSomePuppies(String[][] somePuppies) {
        kennelPuppies = Arrays.copyOf(kennelPuppies, kennelPuppies.length + somePuppies.length);
        for (String[] puppie : somePuppies) {
            kennelPuppies[countPuppies] = puppie;
            countPuppies++;
        }
    }

    public static int getPuppyCount() {
        return countPuppies;
    }

    public static boolean isContainPuppy(String name) {
        for (int i = 0; i < kennelPuppies.length; i++) {
            if (kennelPuppies[i][0].equals(name)) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return kennelPuppies;
    }

    public static String[] getNamesByBreed(String breed) {
        String[] namesOfPuppies = new String[countPuppies];
        int counter = 0;
        for (int i = 0; i < kennelPuppies.length; i++) {
            if (kennelPuppies[i][1].equals(breed)) {
                namesOfPuppies[counter] = kennelPuppies[i][0];
                counter++;
            }
        }
        namesOfPuppies = Arrays.copyOf(namesOfPuppies, counter);
        return namesOfPuppies;
    }

    public static void resetKennel() {
        kennelPuppies = new String[][]{};
        countPuppies = 0;
    }
}
// END
