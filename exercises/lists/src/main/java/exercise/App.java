package exercise;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

// BEGIN
class App {
    public static boolean scrabble(String letters, String word) {
        List<String> charsLetters = new ArrayList<>(List.of(letters.toLowerCase().split("")));

        for (int i = 0; i < word.length(); i++) {
            Character charToRemove = Character.toLowerCase(word.charAt(i));
            boolean isRemoved = charsLetters.remove(charToRemove.toString());
            if (!isRemoved) {
                return false;
            }
        }
        return true;
    }
}
//END
