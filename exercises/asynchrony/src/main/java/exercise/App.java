package exercise;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.CompletableFuture;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.ExecutionException;

class App {
    // BEGIN
    public static CompletableFuture<String> unionFiles(String firstPath,
                                                       String secondPath,
                                                       String destPath) throws ExecutionException, InterruptedException {
        CompletableFuture<String> firstFileResult = readFile(firstPath);
        CompletableFuture<String> secondFileResult = readFile(secondPath);

        firstFileResult.get();
        secondFileResult.get();

        return firstFileResult.thenCombine(secondFileResult, (first, second) -> {
            String fileName = Paths.get(destPath).toAbsolutePath().normalize().toString();
            File destFile = new File(fileName);

            if (!destFile.exists()) {
                try {
                    destFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                FileWriter fw = new FileWriter(fileName);
                PrintWriter pw = new PrintWriter(fw);
                pw.print(first);
                pw.print(second);
                fw.close();
                pw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "OK";
        });
    }

    private static CompletableFuture<String> readFile(String path) {
        CompletableFuture<String> readResult = CompletableFuture.supplyAsync(() -> {
            String result = "";

            try {
                result = Files.readString(Paths.get(path).toAbsolutePath().normalize());
            } catch (IOException e) {
                System.out.println(e);
            }

            return result;
        });

        return readResult;
    }
    // END

    public static void main(String[] args) throws Exception {
        // BEGIN
        CompletableFuture<String> res = unionFiles("src/main/resources/file1.txt",
                "src/main/resources/file2.txt",
                "src/main/resources/result.txt");
        // END
    }
}

