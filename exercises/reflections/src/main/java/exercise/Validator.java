package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

// BEGIN
public class Validator {
    public static List<String> validate(Address address) {
        List<String> notValidFields = new ArrayList<>();
        try {
            for (Field field : address.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(NotNull.class) && field.get(address) == null) {
                    notValidFields.add(field.getName() );
                }
            }
        } catch (IllegalAccessException exception) {
            exception.printStackTrace();
        }
        return notValidFields;
    }
}
// END
