package exercise;

import java.util.*;

// BEGIN
public class SingleTag extends Tag {
    private final String htmlTag = "<%s%s>";
    public SingleTag(String tagName, Map<String, String> attributes) {
        super(tagName, attributes);
    }

    @Override
    public String toString() {
        return String.format(htmlTag, tagName, stringifyAttributes());
    }
}
// END
