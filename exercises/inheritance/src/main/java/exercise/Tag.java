package exercise;

import java.util.List;
import java.util.Map;
// BEGIN
public abstract class Tag {
    String tagName;
    Map<String, String> attributes;

    public Tag(String tagName, Map<String, String> attributes) {
        this.tagName = tagName;
        this.attributes = attributes;
    }

    public String stringifyAttributes() {
        if (!attributes.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append(" ");
            for (Map.Entry<String, String> entry : attributes.entrySet()) {
                sb.append(entry.getKey()).append("=\"").append(entry.getValue()).append("\"").append(" ");
            }
            return sb.deleteCharAt(sb.lastIndexOf(" ")).toString();
        }
        return new String();
    }

    public abstract String toString();
}
// END
