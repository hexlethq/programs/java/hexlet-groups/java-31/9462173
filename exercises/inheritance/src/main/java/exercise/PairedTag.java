package exercise;

import java.util.*;
// BEGIN
public class PairedTag extends Tag {
    private final String tag = "<%s%s>%s%s</%s>";

    private String body;
    private List<Tag> children;

    public PairedTag(String tagName, Map<String, String> attributes, String body, List<Tag> children) {
        super(tagName, attributes);
        this.body = body;
        this.children = children;
    }

    @Override
    public String toString() {
            return String.format(tag, tagName, stringifyAttributes(), stringifyChildren(children), body, tagName);
    }

    public static String stringifyChildren(List<Tag> children) {
        StringBuilder sb = new StringBuilder();
        for (Tag t : children) {
            sb.append(t);
        }
        return sb.toString();
    }
}
// END
