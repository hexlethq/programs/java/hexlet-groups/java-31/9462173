package exercise;

import java.util.Locale;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        if (sentence.contains("!")) {
            System.out.println(sentence.toUpperCase());
        } else {
            System.out.println(sentence.toLowerCase());
        }
        // END
    }
}
