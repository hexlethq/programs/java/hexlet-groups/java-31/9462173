package exercise;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// BEGIN
@RestController
@RequestMapping("/")
public class WelcomeController {
    @GetMapping()
    public String greetSpring() {
        return "Welcome to Spring";
    }

    @GetMapping("hello")
    public String greetCurrentUser(@RequestParam(required = false, defaultValue = "World") String name) {
        return "Hello, " + name;
    }
}
// END
