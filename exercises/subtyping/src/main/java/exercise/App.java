package exercise;

import java.util.HashMap;

import java.util.Map;


// BEGIN
class App {
    public static void swapKeyValue(KeyValueStorage database) {
        for (Map.Entry<String, String> entry : database.toMap().entrySet()) {
            database.unset(entry.getKey());
            database.set(entry.getValue(), entry.getKey());
        }
    }
}
// END
