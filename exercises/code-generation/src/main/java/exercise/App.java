package exercise;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;

// BEGIN
public class App {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    public static String save(Path path, Car car) throws IOException {
        String temp = objectMapper.writeValueAsString(car);
        return String.valueOf(Files.writeString(path, temp));
    }

    public static Car extract(Path path) throws IOException {
        return objectMapper.readValue(Files.readString(path), Car.class);
    }
}
// END
