package exercise;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Value;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

// BEGIN
@AllArgsConstructor
@Getter
// END
class Car {
    int id;
    String brand;
    String model;
    String color;
    User owner;

    // BEGIN
    private static final ObjectMapper objectMapper = new ObjectMapper();
    public String serialize() throws JsonProcessingException {
        return objectMapper.writeValueAsString(Car.class);
    }

    public static Car unserialize(String car) throws IOException {
        return objectMapper.readValue(car, Car.class);
    }
    // END
}
