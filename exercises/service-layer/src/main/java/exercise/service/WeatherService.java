package exercise.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import exercise.HttpClient;
import exercise.model.City;
import org.springframework.stereotype.Service;
import exercise.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service
public class WeatherService {

    @Autowired
    CityRepository cityRepository;

    // Клиент
    HttpClient client;

    // При создании класса сервиса клиент передаётся снаружи
    // В теории это позволит заменить клиент без изменения самого сервиса
    WeatherService(HttpClient client) {
        this.client = client;
    }

    // BEGIN
        public String findOutWeatherInCity(String city) throws JsonProcessingException {
            return client.get("http://weather/api/v2/cities/" + city);
        }

        public List<Map<String, String>> findOutWeatherInSortedCities(String name) throws JsonProcessingException {
            ObjectMapper objectMapper = new ObjectMapper();
            List<Map<String, String>> weather = new ArrayList<>();
            if (name != null) {
                List<City> cities = cityRepository.findAllByNameStartsWithIgnoreCase(name);
                System.out.println(cities);
                for (City city : cities) {
                    weather.add(objectMapper.readValue(
                           findOutWeatherInCity(city.getName()), Map.class));
                }
                return weather;
            }
            List<City> sortedCities = cityRepository.findAllByOrderByNameAsc();
            for (City city : sortedCities) {
                weather.add(objectMapper.readValue(
                        findOutWeatherInCity(city.getName()), Map.class));
            }
            return weather;
        }

    // END
}

