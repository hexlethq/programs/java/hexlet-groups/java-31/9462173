package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
 class App {
    public static Map getWordCount(String str) {
        Map<String, Integer> mapa = new HashMap<>();
        String[] words = str.split(" ");
        if (str.isEmpty()) {
            return mapa;
        }
        for (String word : words) {
            if (mapa.containsKey(word)) {
                mapa.put(word, mapa.get(word) + 1);
            } else {
                mapa.put(word, 1);
            }
        }
        return mapa;
    }

    public static String toString(Map<String, Integer> map) {
        StringBuilder sb = new StringBuilder();
        if (map.size() == 0) {
            return "{}";
        }
        sb.append("{\n");
        for (Map.Entry<String, Integer> str : map.entrySet()) {
            sb.append("  " + str.toString().replace("=", ": ") + "\n");

        }
        sb.append("}");
        return String.valueOf(sb);
    }
}
//END
