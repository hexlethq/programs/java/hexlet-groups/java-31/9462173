package exercise;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Map.Entry;

// BEGIN
class App {
    public static List<Map<String, String>> findWhere(List<Map<String, String>> books, Map<String, String> mapa) {
        List<Map<String, String>> booksResult = new ArrayList<>();
        for (Map<String, String> book : books) {
            if (book.get("author").equals(mapa.get("author")) && book.get("year").equals(mapa.get("year"))) {
                booksResult.add(book);
            } else if (book.get("title").equals(mapa.get("title"))) {
                return booksResult;
            }
        }
        return booksResult;
    }
}
//END
