package exercise;

// BEGIN
class Point {
    private Integer x;
    private Integer y;
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
// END
