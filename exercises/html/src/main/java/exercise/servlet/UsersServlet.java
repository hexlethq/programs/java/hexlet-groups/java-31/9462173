package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

import static com.fasterxml.jackson.databind.type.LogicalType.Map;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        ObjectMapper objectMapper = new ObjectMapper();
        Path path = Paths.get("src/main/resources/users.json").toAbsolutePath().normalize();
        String file = Files.readString(path);
        return objectMapper.readValue(file,  new TypeReference<List<Map<String, String>>>(){});
        // END
    }

    private void showUsers(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException {
        // BEGIN

        List<Map<String, String>> users = getUsers();
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("""
                <html>
                    <head>
                        <body>
                            <table>
                                <tr>
                """);
        for (Map<String, String> user : users) {
            stringBuilder.append("<td>").append(user.get("id")).append("</td>");
            stringBuilder.append("<td>").append("<a href=\"/users/").append(user.get("id")).append("\">")
                    .append(user.get("firstName")).append(" ").append(user.get("lastName"))
                    .append("</a>").append("</td>");
        }
        stringBuilder.append("""
                </tr>
                    </table>
                        </body>
                            </head>
                                </html>
                """);

        PrintWriter printWriter = response.getWriter();
        printWriter.write(stringBuilder.toString());
        printWriter.close();
        // END
    }

    private void showUser(HttpServletRequest request,
                         HttpServletResponse response,
                         String id)
                 throws IOException {

        // BEGIN
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("""
                <html>
                    <head>
                        <body>
                            <table>
                                <tr>
                """);
        List<Map<String, String>> users = getUsers();
        List<String> temp = new ArrayList<>();
        for (Map<String, String> user : users) {
            if (user.get("id").equals(id)) {
                temp.add(user.toString());
                stringBuilder.append("<td>").append(user.get("id")).append("</td>");
                stringBuilder.append("<td>").append(user.get("firstName")).append("</td>");
                stringBuilder.append("<td>").append(user.get("lastName")).append("</td>");
                stringBuilder.append("<td>").append(user.get("email")).append("</td>");

            }
            }
        if (temp.isEmpty()) {
            response.sendError(404);
        }
        stringBuilder.append("""
                </tr>
                    </table>
                        </body>
                            </head>
                                </html>
                """);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter printWriter = response.getWriter();
        printWriter.write(stringBuilder.toString());
        printWriter.close();
        // END
    }
}
