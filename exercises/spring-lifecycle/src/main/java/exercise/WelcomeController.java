package exercise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

// BEGIN
@RestController
public class WelcomeController {
    @Autowired
    Meal meal;

    @Autowired
    MyApplicationConfig myApplicationConfig;

    @GetMapping("/daytime")
    public String getDayTime() {
        String daytime = myApplicationConfig.getCurrentTime().getName();
        return "It is " + daytime.toLowerCase()
                + " now. Enjoy your " + meal.getMealForDaytime(daytime.toLowerCase());
    }
}
// END
