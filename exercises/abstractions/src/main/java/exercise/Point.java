package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        int[] arr = new int[]{x, y};
        return arr;
    }

    public static int getX(int[] points) {
        int pointX = points[0];
        return pointX;
    }

    public static int getY(int[] points) {
        int pointY = points[1];
        return pointY;
    }

    public static String pointToString(int[] points) {
        return "(" + points[0] + ", " + points[1] + ")";
    }

    public static int getQuadrant(int[] points) {
        int x = points[0];
        int y = points[1];

        if (x > 0 && y > 0) {
            return 1;
        } else if (x < 0 && y > 0) {
            return 2;
        } else if (x < 0 && y < 0) {
            return 3;
        } else if (x > 0 && y < 0) {
            return 4;
        } else {
            return 0;
        }
    }
    // END
}
