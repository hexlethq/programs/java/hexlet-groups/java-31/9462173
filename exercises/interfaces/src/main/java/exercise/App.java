package exercise;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
class App {
    public static List<String> buildAppartmentsList(List<Home> apartments, int ammount) {
        List<String> sortedApartments = apartments.stream()
                .sorted(Comparator.comparingDouble(Home::getArea))
                .map(x -> x.toString())
                .limit(ammount)
                .collect(Collectors.toList());

        return sortedApartments;
    }
}
// END
