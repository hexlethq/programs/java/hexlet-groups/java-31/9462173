package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] reverse(int[] arrNumbers) {
        if (arrNumbers == null) {
            return arrNumbers;
        }
        int[] res = new int[arrNumbers.length];
        for (int i = 0 ;i < arrNumbers.length; i++) {
            res[i]= arrNumbers[arrNumbers.length - 1 - i];
        }
        return res;
    }

    public static int mult(int[] arrayNum) {
        int arrProduct = 1;
        for (int eachNum : arrayNum) {
            arrProduct *= eachNum;
        }
        return arrProduct;
    }
    // END
}
