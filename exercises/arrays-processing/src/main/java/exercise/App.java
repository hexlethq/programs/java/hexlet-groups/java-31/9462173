package exercise;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] numbers) {
        int res = Integer.MIN_VALUE;
        int resindex = -1;
        for (int i = 0; i < numbers.length; i++) {
            if (res < numbers[i] && numbers[i] < 0) {
                res = numbers[i];
                resindex = i;
            }
        }
        return resindex;
    }

    public static int[] getElementsLessAverage(int[] arrNumbers) {

        if (arrNumbers.length == 0) {
            return arrNumbers;
        }

        int avgValue = calculateAverage(arrNumbers);
        int counter = 0;
        for (int i = 0; i < arrNumbers.length; i++) {
            if (arrNumbers[i] <= avgValue) {
                counter++;
            }
        }

        int[] result = new int[counter];
        counter = 0;
        for (int i = 0; i < arrNumbers.length; i++) {
            if (arrNumbers[i] <= avgValue) {
                result[counter] = arrNumbers[i];
                counter++;
            }
        }
        return result;
    }

    public static int calculateAverage(int[] arrayNumbers) {
        int sum = 0;
        int averageValue = 0;

        for (int i = 0; i < arrayNumbers.length; i++) {
            sum += arrayNumbers[i];
        }
        averageValue = sum / arrayNumbers.length;
        return averageValue;
    }
    // END
}
