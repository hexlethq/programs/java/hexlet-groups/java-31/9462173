// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;


class App {
    public static double[] getMidpointOfSegment(double[][] segment) {
        double pointX = (segment[0][0] + segment[1][0]) / 2;
        double pointY = (segment[0][1] + segment[1][1]) / 2;
        double[] middleOfSegment = new double[]{pointX, pointY};
        return middleOfSegment;
    }

    public static double[][] reverse(double[][] segment) {
        double[] beginPoint = new double[]{segment[1][0], segment[1][1]};
        double[] endPoint = new double[]{segment[0][0], segment[0][1]};
        double[][] reversedPoints = new double[][]{beginPoint, endPoint};
        return reversedPoints;
    }
}
// END
