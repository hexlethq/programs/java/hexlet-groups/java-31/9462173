package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

class App {
    // BEGIN
    public static String buildList(String[] array) {
        StringBuilder sb = new StringBuilder();
        sb.append("<ul>\n");
        if (array.length == 0) {
            return "";
        }
        for (String str : array) {
            sb.append("  <li>" + str + "</li>\n");
        }
        sb.append("</ul>");
        return String.valueOf(sb);
    }

    public static String getUsersByYear(String[][] users, int date) {
        String res = "";
        boolean isEmpty = true;
        StringBuilder sb = new StringBuilder();
        sb.append("<ul>\n");
        for (int i = 0; i < users.length; i++) {
            res = users[i][1].substring(0, 4);
            int bday = Integer.parseInt(res);
            if (date == bday) {
                sb.append("  <li>" + users[i][0] + "</li>\n");
                isEmpty = false;
            }
        }
        if (isEmpty) {
            return "";
        }
        sb.append("</ul>");
        return String.valueOf(sb);
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        return null;
        // END
    }
}
