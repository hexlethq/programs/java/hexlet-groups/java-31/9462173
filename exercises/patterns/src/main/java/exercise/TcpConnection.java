package exercise;
import exercise.connections.Connection;
import exercise.connections.Disconnected;

// BEGIN
public class TcpConnection {
    private String ip;
    private int port;
    public String clipboard;
    public Connection connectionState;

    public TcpConnection(String ip, int port) {
        this.connectionState = new Disconnected(this);
        this.ip = ip;
        this.port = port;
    }

    public String getCurrentState() {
        return connectionState.getCurrentState();
    }

    public void connect() {
        connectionState.connect();
    }

    public void disconnect() {
        connectionState.disconnect();
    }

    public void write(String text) {
        connectionState.write(text);
    }


}
// END

