package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;

import java.sql.*;
import java.util.*;

import org.apache.commons.lang3.ArrayUtils;

import exercise.TemplateEngineUtil;


public class ArticlesServlet extends HttpServlet {

    private String getId(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return null;
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 1, null);
    }

    private String getAction(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return "list";
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 2, getId(request));
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String action = getAction(request);

        switch (action) {
            case "list":
                showArticles(request, response);
                break;
            default:
                showArticle(request, response);
                break;
        }
    }

    private void showArticles(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM articles ORDER BY id LIMIT 10 OFFSET ?");
            int page = getPage(request);
            int startPoint = (page * 10) - 10;
            preparedStatement.setInt(1, startPoint);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Map<String, Object>> articles = new ArrayList<>();
            while (resultSet.next()) {
                articles.add(Map.of("id", resultSet.getLong("id"),
                        "title", resultSet.getString("title"),
                        "body", resultSet.getString("body")));
            }
            resultSet.close();
            preparedStatement.close();
            request.setAttribute("articles", articles);
            request.setAttribute("page", page);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        // END
        TemplateEngineUtil.render("articles/index.html", request, response);
    }

    private void showArticle(HttpServletRequest request,
                         HttpServletResponse response)
                 throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT title, body FROM articles WHERE id = ?");
            int id = Integer.parseInt(getId(request));
            if (id > 100) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                request.setAttribute("title", resultSet.getString("title"));
                request.setAttribute("body", resultSet.getString("body"));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // END
        TemplateEngineUtil.render("articles/show.html", request, response);
    }

    private int getPage(HttpServletRequest request) {
        String page = request.getParameter("page");
        if (page != null) {
            return Integer.parseInt(page);
        }
        return 1;
    }
}
