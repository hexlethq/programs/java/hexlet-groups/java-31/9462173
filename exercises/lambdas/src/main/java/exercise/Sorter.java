package exercise;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.List;
import java.time.LocalDate;
import java.util.stream.Collectors;

// BEGIN
class Sorter {
    public static List<Map<String, String>> takeOldestMans(List<Map<String, String>> list) {
        return list.stream()
                .filter(x -> x.get("gender").equals("male"))
                .sorted(Collections.reverseOrder(Comparator.comparing(x -> x.get("birthday"))))
                .collect(Collectors.toList());
    }
}
// END
