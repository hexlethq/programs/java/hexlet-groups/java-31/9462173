package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
class App {
    public static String[][] enlargeArrayImage(String[][] image) {
        String[][] result = Arrays.stream(image)
                .flatMap(x -> Stream.of(x, x))
                .map(x -> Arrays.stream(x)
                        .flatMap(y -> Stream.of(y, y)))
                .map(x -> x.toArray(String[]::new))
                .toArray(String[][]::new);
        return result;
    }
}
// END
