package exercise;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

// BEGIN
class SorterTest {
    @Test
    void testTakeOldestMans() {
        List<Map<String, String>> expected = List.of(
                Map.of("name", "Vladimir Nikolaev", "birthday", "1990-12-27", "gender", "male"),
                Map.of("name", "Andrey Petrov", "birthday", "1989-11-23", "gender", "male"),
                Map.of("name", "John Smith", "birthday", "1989-03-11", "gender", "male")
        );
        List<Map<String, String>> actual = Sorter.takeOldestMans(expected);
        Assertions.assertThat(actual.equals(expected));
    }
}
// END


