package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(double side1, double side2, double angle) {
        double radian = (angle * 3.14) / 180;
        double res = ((side1 * side2) / 2) * Math.sin(radian);
        return res;
    };
    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45 ));
    };
    // END
}
