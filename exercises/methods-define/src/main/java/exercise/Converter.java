package exercise;

class Converter {
    // BEGIN
    public static int convert(int number, String value ) {
        if (value.equals("b")) {
            return number * 1024;
        }
        else if (value.equals("Kb")) {
            return number / 1024;
        }
        else {
            return number = 0;
        }
    };

    public static void main(String[] args) {
        System.out.println("10 Kb = " + convert (10, "b") + " b");
    };
    // END
}
