package exercise;

import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


import exercise.calculator.CalculatorImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

// BEGIN
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {
    private Map<String, String> beanInfo = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean.getClass().isAnnotationPresent(Inspect.class)) {
            Inspect annotation = bean.getClass().getAnnotation(Inspect.class);
            beanInfo.put(beanName, annotation.level());
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (!beanInfo.containsKey(beanName)) {
            return bean;
        }
        final Logger logger = LoggerFactory.getLogger(beanName);
        String levelLog = beanInfo.get(beanName);
        Object proxyInstance = Proxy.newProxyInstance(
                CalculatorImpl.class.getClassLoader(),
                CalculatorImpl.class.getInterfaces(),
                (proxy, method, args) -> {
                    if (levelLog.equals("info")) {
                        logger.info("Was called method: " + method.getName() + "() with arguments: "
                                + Arrays.toString(args));
                    }
                    if (levelLog.equals("debug")) {
                        logger.debug("Was called method: " + method.getName() + "() with arguments: "
                                + Arrays.toString(args));
                    }
                    return method.invoke(bean, args);
                }
        );
        return proxyInstance;
    }
}
// END
