package exercise;

import java.util.Arrays;

// BEGIN
public class MinThread extends Thread {
    private int[] arr;
    private static Integer min;

    public MinThread(int[] arr) {
        this.arr = arr;
    }

    @Override
    public void run() {
        min = Arrays.stream(arr)
                .min().getAsInt();
    }

    public static Integer getMin() {
        return min;
    }
}
// END
