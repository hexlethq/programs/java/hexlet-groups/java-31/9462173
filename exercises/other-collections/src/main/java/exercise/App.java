package exercise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

// BEGIN
class App {
    public static Map<String, String> genDiff(Map<String, Object> firstMapa, Map<String, Object> secondMapa) {
        Map<String, String> result = new LinkedHashMap<>();
        for (Map.Entry<String, Object> entry : secondMapa.entrySet()) {
            if (!firstMapa.containsKey(entry.getKey())) {
                result.put(entry.getKey(), "added");
            }
            if (firstMapa.containsKey(entry.getKey()) && !firstMapa.containsValue(entry.getValue())) {
                result.put(entry.getKey(), "changed");
            }
            if (firstMapa.containsKey(entry.getKey()) && firstMapa.containsValue(entry.getValue())) {
                result.put(entry.getKey(), "unchanged");
            }
        }
        for (Map.Entry<String, Object> entry : firstMapa.entrySet()) {
            if (!secondMapa.containsKey(entry.getKey())) {
                result.put(entry.getKey(), "deleted");
            }
        }
        return result;
    }
}
//END
