package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class AppTest {
    List<Integer> list;


    @Test
    void testTake() {
        // BEGIN
        List<Integer> abstractElements = List.of(1, 2, 3, 4, 5, 6, 7);
        int listSize = 7;
        List<Integer> actualMethodTakeList = App.take(abstractElements, listSize);
        Assertions.assertThat(actualMethodTakeList.size())
                .isEqualTo(abstractElements.size());
        // END
    }
}
