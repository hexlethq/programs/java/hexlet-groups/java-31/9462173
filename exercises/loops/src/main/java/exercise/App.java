package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String phrase) {
        String fixedText = phrase.toUpperCase().trim().replaceAll(" +", " ");
        String res = "";
        for (int i = 0; i < fixedText.length(); i++) {
            char symbol = fixedText.charAt(i);
            if (symbol == ' ') {
                res += fixedText.charAt(i + 1);
            }
        }
        return fixedText.charAt(0) + res;
    };
    // END
}
