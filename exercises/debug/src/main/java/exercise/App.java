package exercise;

class App {
    public static String getTypeOfTriangle(int side1, int side2, int side3) {
        if (side1 <= 1 || side2 <= 1 || side3 <= 1) {
            return "Треугольник не существует";
        } else if (side1 == side2 && side1 != side3 || side2 == side3 && side2 != side1 || side1 == side3 && side1 != side2) {
            return "Равнобедренный";
        } else if (side1 == side2 && side2 == side3 && side3 == side1) {
            return "Равносторонний";
        } else if (side1 != side2 && side2 != side3 && side3 != side1) {
            return "Разносторонний";
        }
        return null;
    };
}
